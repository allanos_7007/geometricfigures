﻿/*
    1 - a) Agregar figuras
    Para agregar alguna figura, tenemos que crear su propia clase y heredar de la clase base 'FiguraGeometricaBase'.
    La clase base nos obliga a implementar los metodos necesarios para brindar informacion de cada figura y asi
    poder generar el reporte.

    1 - b) Agregar Datos de la figura al archivo de idiomas
    Cada figura tiene una seccion dentro del archivo de Idiomas dentro de la seccion "Figures". El nombre de
    esta nueva seccion tiene que ser el mismo que el nombre de la class de mi nueva figura. Ej: Si implmento
    la figura "parallelogram" la nueva seccion quedaria de la siguiente forma.
    
    "Figures": {
      .................. Figuras anteriores .................
      "Parallelogram": {
        "Plural": "{quantity} Paralelogramos | Area {area} | Perimetro {perimeter} <br/>",
        "Singular": "{quantity} Paralelogramos | Area {area} | Perimetro {perimeter} <br/>"
      }
    }

    y mi class deberia quedar asi:

    publi class Parallelogram : FiguraGeometricaBase
    {
        public override double CalculateArea()
        {
            // Implement
        }

        public override double CalculatePerimeter()
        {
            // Implement
        } 
    }
    

    2 - Agregar Idioma
    Para agregar un idioma, tenemos que agregar un elemento al enum "eLanguage", y para alimentar este idioma 
    tenemos que agregar un archivo con el mismo nombre que nuestro elemento. Ejemplo si creamos un elemente 'br'
    nuestro archivo se tiene q llamar "br.json". El archivo tiene q respetar las mas propiedades que tienen los
    otros archivos de idioma, por ej el archivo "ar.json". La ubicacion del archivo tiene que respetar el mimsmo
    nivel de gerarquia que los anteriores, ej "ar.json".

    ------------- Aclaracion -------------
    - Si por alguna razon no levanta ningun archivo de idioma, siempre verificar la propiedad "Copy to Output Directory",
      tiene que tener como valor "Copy always"
    - Si no veen reflejado el mismo valor que el archivo de idiomas, siendo que fue modificado y guardado, verificar que
      el archivo este cerrado en VS y luego hacer un "re-build" de la solucion.

 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Classes
{
    public interface IFiguraGeometricaBase {
        double CalculateArea();
        double CalculatePerimeter();
    }
    public abstract class FiguraGeometricaBase : IFiguraGeometricaBase
    {
        public double Lado { get; set; }

        public abstract double CalculateArea();

        public abstract double CalculatePerimeter();

        protected double RoundValueAndAdd(double value)
        {
            Decimal decValue = (decimal)value;

            return Convert.ToDouble(Math.Round(decValue, 2));
        }
    }

    public class Scuare : FiguraGeometricaBase
    {

        public override double CalculateArea()
        {
            return base.RoundValueAndAdd(Lado * Lado);
        }

        public override double CalculatePerimeter()
        {
            return base.RoundValueAndAdd(Lado * 4);
        }
    }

    public class Circle : FiguraGeometricaBase
    {
        public override double CalculateArea()
        {
            return base.RoundValueAndAdd(Math.PI * (Lado / 2) * (Lado / 2)) ;
        }

        public override double CalculatePerimeter()
        {
            return base.RoundValueAndAdd(Math.PI * Lado);
        }
    }

    public class EquilateralTriangle : FiguraGeometricaBase
    {
        public override double CalculateArea()
        {
            return base.RoundValueAndAdd((Math.Sqrt(3) / 4) * Lado * Lado);
        }

        public override double CalculatePerimeter()
        {
            return base.RoundValueAndAdd(Lado * 3);
        }
    }

    public class Trapeze : FiguraGeometricaBase
    {
        public double BaseMayor { get; set; }
        public double BaseMenor { get; set; }
        public double Altura { get; set; }
        public override double CalculateArea()
        {
            return base.RoundValueAndAdd(((BaseMayor + BaseMenor) * Altura) / 2);
        }

        public override double CalculatePerimeter()
        {
            return base.RoundValueAndAdd(Lado * 2 + BaseMayor + BaseMenor);
        }
    }

    public class Rectangle : FiguraGeometricaBase
    {
        public double Altura { get; set; }
        public override double CalculateArea()
        {
            return base.RoundValueAndAdd(Altura * Lado);
        }
        public override double CalculatePerimeter()
        {
            return base.RoundValueAndAdd(Lado * 2 + Altura * 2);
        }
    }

    public enum eLanguage
    {
        ar,
        en,
        br,
        it
    }

    public class FormaGeometricaV2
    {
        public dynamic Translate { get; set; }

        public StringBuilder Imprimir(ICollection<FiguraGeometricaBase> figuras, eLanguage lenguaje)
        {
            try
            {
                var path = AppDomain.CurrentDomain.BaseDirectory + $"Classes\\{(eLanguage)lenguaje}.json";
                var file = System.IO.File.ReadAllText(path, Encoding.Default);
                Translate = Newtonsoft.Json.JsonConvert.DeserializeObject(file);
            }
            catch (Exception)
            {

                return new StringBuilder("File language not exist!");
            }
            try
            {
                if (!figuras.Any())
                {
                    return new StringBuilder(Translate.Report.Empty.Value);
                }
                else
                {
                    var report = this.CreateReporte(figuras);
                    return report;
                }
            }
            catch (Exception ex)
            {
                return new StringBuilder(Translate.Report.Error.Value);
            }
        }

        private StringBuilder CreateReporte(ICollection<FiguraGeometricaBase> figuras)
        {
            StringBuilder reporte = new StringBuilder();
            reporte.Append(Translate.Report.Title.Value);

            var figurasSplit = this.SplitGeometric(figuras);
            double totalArea = 0;
            double totalPerimetro = 0;

            foreach (var item in figurasSplit)
            {
                double cantidad = item.Value.Count;
                double area = 0;
                double perimetro = 0;
                string name = item.Key;
                var g = Translate.Report.Figures.name;
                foreach (var figuraGeometrica in item.Value)
                {
                    area += figuraGeometrica.CalculateArea();
                    perimetro += figuraGeometrica.CalculatePerimeter();
                }
                string line = cantidad > 1 ? Translate.Report.Figures[name].Plural.Value : Translate.Report.Figures[name].Singular.Value;
                line = line.Replace("{quantity}", cantidad.ToString())
                           .Replace("{area}", area.ToString())
                           .Replace("{perimeter}", perimetro.ToString());
                reporte.Append(line);
                totalArea += area;
                totalPerimetro += perimetro;
            }

            reporte.Append(Translate.Report.Resumen.Total.Value);
            reporte.Append(figuras.Count + " " + Translate.Report.Resumen.Shapes.Value);
            reporte.Append(" " + Translate.Report.Resumen.Perimeter.Value + " " + (totalPerimetro).ToString("#.##"));
            reporte.Append(" " + Translate.Report.Resumen.Area.Value + " " + (totalArea).ToString("#.##"));

            return reporte;
        }


        private IDictionary<string, ICollection<FiguraGeometricaBase>> SplitGeometric(ICollection<FiguraGeometricaBase> figuras)
        {
            IDictionary<string, ICollection<FiguraGeometricaBase>> groupByIntanceOf = new Dictionary<string, ICollection<FiguraGeometricaBase>>();

            foreach (var item in figuras)
            {
                if (groupByIntanceOf.ContainsKey(item.GetType().Name))
                {
                    var element = groupByIntanceOf[item.GetType().Name];
                    element.Add(item);
                    groupByIntanceOf[item.GetType().Name] = element;
                }
                else
                {
                    groupByIntanceOf.Add(item.GetType().Name, new List<FiguraGeometricaBase>() { item });
                }
            }

            return groupByIntanceOf;
        }
    }
}
