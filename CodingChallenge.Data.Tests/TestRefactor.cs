﻿using CodingChallenge.Data.Classes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class TestRefactor
    {
        [TestCase]
        public void TestFileLenguageNoExit()
        {
            var resultMock = "File language not exist!";
            var cuadrados = new List<FiguraGeometricaBase>
            {
                new Scuare(){ Lado = 5},
                new Scuare(){ Lado = 1},
                new Scuare(){ Lado = 3}
            };
            var fGeometricaMock = new FormaGeometricaV2();
            var resumen = fGeometricaMock.Imprimir(cuadrados, eLanguage.br).ToString();

            Assert.AreEqual(resultMock, resumen);
        }

        [TestCase]
        public void TestInternalErrorAr()
        {
            var resultMock = "<h1>Error interno, re-intente luego!</h1>";
            var cuadrados = new List<FiguraGeometricaBase>
            {
                new Circle(){ Lado = double.MaxValue},
                new Scuare(){ Lado = double.MaxValue},
                new Scuare(){ Lado = double.MaxValue}
            };
            var fGeometricaMock = new FormaGeometricaV2();
            var resumen = fGeometricaMock.Imprimir(cuadrados, eLanguage.ar).ToString();

            Assert.AreEqual(resultMock, resumen);
        }

        [TestCase]
        public void TestInternalErrorItaliano()
        {
            var resultMock = "<h1> Errore interno, riprova più tardi! </h1>";
            var cuadrados = new List<FiguraGeometricaBase>
            {
                new Circle(){ Lado = double.MaxValue},
                new Scuare(){ Lado = double.MaxValue},
                new Scuare(){ Lado = double.MaxValue}
            };
            var fGeometricaMock = new FormaGeometricaV2();
            var resumen = fGeometricaMock.Imprimir(cuadrados, eLanguage.it).ToString();

            Assert.AreEqual(resultMock, resumen);
        }

        public void TestInternalErrorEn()
        {
            var resultMock = "<h1>Error interno, re-intente luego!</h1>";
            var cuadrados = new List<FiguraGeometricaBase>
            {
                new Circle(){ Lado = double.MaxValue},
                new Scuare(){ Lado = double.MaxValue},
                new Scuare(){ Lado = double.MaxValue}
            };
            var fGeometricaMock = new FormaGeometricaV2();
            var resumen = fGeometricaMock.Imprimir(cuadrados, eLanguage.en).ToString();

            Assert.AreEqual(resultMock, resumen);
        }

        [TestCase]
        public void TestResumenListaConOchoCuadradorYDosTrapecios()
        {
            var requet = new List<FiguraGeometricaBase>() {
                new Scuare() { Lado = 4 },
                new Scuare() { Lado = 4 },
                new Scuare() { Lado = 4 },
                new Trapeze() { Lado = 4,Altura = 5,BaseMayor = 8,BaseMenor = 7 },
                new Trapeze() { Lado = 4,Altura = 5,BaseMayor = 10,BaseMenor = 6 },
                new Scuare() { Lado = 4 },
                new Scuare() { Lado = 4 },
                new Scuare() { Lado = 4 },
                new Scuare() { Lado = 4 },
                new Scuare() { Lado = 4 }
            };

            var fGeometricaMock = new FormaGeometricaV2();
            var resultMock = "<h1>Reporte de Formas</h1>8 Cuadrados | Area 128 | Perimetro 128 <br/>2 Trapecios | Area 77,5 | Perimetro 47 <br/>TOTAL:<br/>10 formas Perimetro 175 Area 205,5";
            var resumen = fGeometricaMock.Imprimir(requet, eLanguage.ar).ToString();
            Assert.AreEqual(resultMock, resumen);
        }

        [TestCase]
        public void TestResumenListaConUnaDeCadaFigura()
        {
            var requet = new List<FiguraGeometricaBase>() {
                new Scuare() { Lado = 4 },
                new Trapeze() { Lado = 4,Altura = 5,BaseMayor = 8,BaseMenor = 7 },
                new Circle(){ Lado = 3 },
                new EquilateralTriangle(){ Lado = 4 },
                new Rectangle() { Lado = 4, Altura = 2 }
            };

            var fGeometricaMock = new FormaGeometricaV2();
            var resultMock = "<h1>Reporte de Formas</h1>1 Cuadrado | Area 16 | Perimetro 16 <br/>1 Trapecio | Area 37,5 | Perimetro 23 <br/>1 Circulo | Area 7,07 | Perimetro 9,42 <br/>1 Triangulo | Area 6,93 | Perimetro 12 <br/>1 Rectangulo | Area 8 | Perimetro 12 <br/>TOTAL:<br/>5 formas Perimetro 72,42 Area 75,5";
            var resumen = fGeometricaMock.Imprimir(requet, eLanguage.ar).ToString();
            Assert.AreEqual(resultMock, resumen);
        }

        #region [Mismo unittest que anterior Version]

        [TestCase]
        public void TestResumenListaVacia()
        {
            var resultMock = "<h1>Lista vacía de formas!</h1>";
            var fGeometricaMock = new FormaGeometricaV2();
            var resumen = fGeometricaMock.Imprimir(new List<FiguraGeometricaBase>(), eLanguage.ar).ToString();

            Assert.AreEqual(resultMock, resumen);
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            var resultMock = "<h1>Empty list of shapes!</h1>";
            var fGeometricaMock = new FormaGeometricaV2();
            var resumen = fGeometricaMock.Imprimir(new List<FiguraGeometricaBase>(), eLanguage.en).ToString();
            Assert.AreEqual(resultMock, resumen);
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var request = new List<FiguraGeometricaBase> { new Scuare() { Lado = 5 } };
            var fGeometricaMock = new FormaGeometricaV2();

            var resultMock = "<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perimetro 20 <br/>TOTAL:<br/>1 formas Perimetro 20 Area 25";
            var resumen = fGeometricaMock.Imprimir(request, eLanguage.ar).ToString();
            Assert.AreEqual(resultMock, resumen);
        }



        [TestCase]
        public void TestResumenListaConUnCuadradoItaliano()
        {
            var request = new List<FiguraGeometricaBase> { new Scuare() { Lado = 5 } };
            var fGeometricaMock = new FormaGeometricaV2();

            var resultMock = "<h1> Rapporto modulo </h1>1 Square | Area 25 | Perimeter 20 <br/>TOTALE:<br/>1 forme Perimetro 20 Area 25";
            var resumen = fGeometricaMock.Imprimir(request, eLanguage.it).ToString();
            Assert.AreEqual(resultMock, resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var fGeometricaMock = new FormaGeometricaV2();
            var cuadrados = new List<FiguraGeometricaBase>
            {
                new Scuare(){ Lado = 5},
                new Scuare(){ Lado = 1},
                new Scuare(){ Lado = 3}
            };

            var resultMock = "<h1>Shapes report</h1>3 Scuares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35";
            var resumen = fGeometricaMock.Imprimir(cuadrados, eLanguage.en).ToString();

            Assert.AreEqual(resultMock, resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var fGeometricaMock = new FormaGeometricaV2();
            var formas = new List<FiguraGeometricaBase>
            {
                new Scuare(){ Lado = 5},
                new Circle(){ Lado = 3 },
                new EquilateralTriangle(){ Lado = 4 },
                new Scuare(){ Lado = 2},
                new EquilateralTriangle(){ Lado = 9 },
                new Circle(){ Lado = (double)2.75m },
                new EquilateralTriangle(){ Lado = (double)4.2m }
            };

            var resultMock = "<h1>Shapes report</h1>2 Scuares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65";
            var resumen = fGeometricaMock.Imprimir(formas, eLanguage.en).ToString();

            Assert.AreEqual(resultMock,resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var fGeometricaMock = new FormaGeometricaV2();
            var formas = new List<FiguraGeometricaBase>
            {
                new Scuare(){ Lado = 5},
                new Circle(){ Lado = 3 },
                new EquilateralTriangle(){ Lado = 4 },
                new Scuare(){ Lado = 2},
                new EquilateralTriangle(){ Lado = 9  },
                new Circle(){ Lado = (double)2.75m },
                new EquilateralTriangle(){ Lado = (double)4.2m }
            };

            var resultMock = "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perimetro 28 <br/>2 Circulos | Area 13,01 | Perimetro 18,06 <br/>3 Triangulos | Area 49,64 | Perimetro 51,6 <br/>TOTAL:<br/>7 formas Perimetro 97,66 Area 91,65";
            var resumen = fGeometricaMock.Imprimir(formas, eLanguage.ar).ToString();

            Assert.AreEqual(resultMock, resumen);
        }

        #endregion
    }
}
